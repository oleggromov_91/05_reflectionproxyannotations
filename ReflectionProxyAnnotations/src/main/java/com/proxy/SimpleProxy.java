package com.proxy;

import lombok.extern.log4j.Log4j;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.function.Supplier;

@Log4j
public class SimpleProxy<T> implements InvocationHandler {

    private final Supplier<T> proxySupplier;

    public SimpleProxy(Supplier<T> supplier) {
        this.proxySupplier = supplier;
    }

    @Override
    public Object invoke(Object o, Method method, Object[] args) throws Throwable {
        log.info("method call was detected: " + method.getName());
        return method.invoke(proxySupplier.get(), args);
    }


    @SuppressWarnings("unchecked")
    public T proxyMethodLogger() {
        return (T) Proxy.newProxyInstance(proxySupplier.get().getClass().getClassLoader(), proxySupplier.get().getClass().getInterfaces(), this);
    }

}