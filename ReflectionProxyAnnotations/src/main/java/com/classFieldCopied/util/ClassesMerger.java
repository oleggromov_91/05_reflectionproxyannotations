package com.classFieldCopied.util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ClassesMerger {


    public static <T> T merge(Object firstObject, Object secondObject, Class<T> clazz) throws IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {

        T resultMerge = clazz.getDeclaredConstructor().newInstance();

        checkSameFieldsForObjects(firstObject, secondObject);

        mergeObjectToObjectFields(firstObject, resultMerge);
        mergeObjectToObjectFields(secondObject, resultMerge);

        return resultMerge;
    }


    private static void mergeObjectToObjectFields(Object from, Object to) {
        Field[] toFields = to.getClass().getDeclaredFields();
        Field[] fromFields = from.getClass().getDeclaredFields();

        for (int i = 0; i < toFields.length; i++) {
            toFields[i].setAccessible(true);
            int index = i;
            Arrays.stream(fromFields).forEach(field ->
            {
                field.setAccessible(true);
                if (toFields[index].getType().equals(field.getType()) & toFields[index].getName().equals(field.getName())) {
                    try {
                        toFields[index].set(to, field.get(from));
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    private static void checkSameFieldsForObjects(Object first, Object second) {
        String[] fields = Stream.concat(Arrays.stream(first.getClass().getDeclaredFields()).map(Field::getName), Arrays.stream(second.getClass().getDeclaredFields()).map(Field::getName))
                .toArray(String[]::new);
        Set<String> uniqFields = Arrays.stream(fields).collect(Collectors.toSet());

        if (fields.length != uniqFields.size()) {
            throw new IllegalArgumentException("Classes has same nameFields");
        }
    }
}
