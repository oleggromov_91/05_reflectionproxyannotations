package com.annotations.validators.basicValidator;

import com.annotations.validators.entityValidator.*;
import com.annotations.validators.entityValidatorAnnotations.*;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Map;

public class BasicEntityValidator implements BasicValidator {

    private Map<Class<? extends Annotation>, EntityValidator> entityAnnotations = Map.of(
            MinNumberValue.class, new MinNumberEntityValidator(),
            MaxNumberValue.class, new MaxNumberEntityValidator(),
            MinLength.class, new MinLengthEntityValidator(),
            MaxLength.class, new MaxLengthEntityValidator(),
            NotNull.class, new NotNullEntityValidator(),
            NotEmpty.class, new NotEmptyEntityValidator()
    );


    public BasicEntityValidator(Map<Class<? extends Annotation>, EntityValidator> entityAnnotations) {
        this.entityAnnotations = entityAnnotations;
    }

    public BasicEntityValidator() {
    }

    @Override
    public boolean validate(Object entity) {
        Class<?> clazz = entity.getClass();
        for (Field entityField : clazz.getDeclaredFields()) {
            entityField.setAccessible(true);
            entityAnnotations.entrySet().stream()
                    .filter(annotations -> entityField.isAnnotationPresent(annotations.getKey()))
                    .map(Map.Entry::getValue)
                    .forEach(entityValidator -> entityValidator.validate(entity, entityField));
        }
        return true;
    }
}
