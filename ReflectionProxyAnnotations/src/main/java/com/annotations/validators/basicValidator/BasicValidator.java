package com.annotations.validators.basicValidator;

public interface BasicValidator {

    boolean validate(Object entity);

}
