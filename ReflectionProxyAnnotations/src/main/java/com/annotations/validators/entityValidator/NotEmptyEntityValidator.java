package com.annotations.validators.entityValidator;

import com.annotations.validators.entityValidatorAnnotations.NotNull;

import java.lang.reflect.Field;

public class NotEmptyEntityValidator implements EntityValidator {

    @Override
    public void validate(Object entity, Field entityField) {
        if (!isFieldIsNull(entity, entityField)) {
            typeShouldStringForAnnotations(entityField, NotNull.class);
            try {
                String entityFieldAnnotate = (String) entityField.get(entity);
                if (entityFieldAnnotate.isBlank()) {
                    throw new IllegalArgumentException("Field of '" + entityField.getName() + "' entity is empty");
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }
}
