package com.annotations.validators.entityValidator;

import com.annotations.validators.entityValidatorAnnotations.NotNull;

import java.lang.reflect.Field;
import java.util.Objects;

public class NotNullEntityValidator implements EntityValidator {

    @Override
    public void validate(Object entity, Field entityField) {
        typeShouldReferenceForAnnotations(entityField, NotNull.class);
        try {
            if (Objects.isNull(entityField.get(entity))) {
                throw new IllegalArgumentException("Field of '" + entityField.getName() + "' entity is null");
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

}
