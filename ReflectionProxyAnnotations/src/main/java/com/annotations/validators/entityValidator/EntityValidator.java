package com.annotations.validators.entityValidator;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Objects;

public interface EntityValidator {

    void validate(Object entity, Field entityField);

    default long parseNumber(String value) {
        String FLOATING_NUMBER_REGEXP = "\\.\\d+";
        return Long.parseLong(value.replaceAll(FLOATING_NUMBER_REGEXP, ""));
    }

    default void typeShouldNumberForAnnotations(Field entityField, Class<? extends Annotation> annotation) {
        if (!entityField.getType().isPrimitive()) {
            if (!entityField.getType().getSuperclass().equals(Number.class)) {
                throw new IllegalArgumentException("Requirement type for annotation '" + annotation.getName() + "' is a number");
            }
        }
    }

    default void typeShouldStringForAnnotations(Field entityField, Class<? extends Annotation> annotation) {
        if (!entityField.getType().equals(String.class)) {
            throw new IllegalArgumentException("Requirement type for annotation '" + annotation.getName() + "' is a string");
        }
    }

    default void typeShouldReferenceForAnnotations(Field entityField, Class<? extends Annotation> annotation) {
        if (entityField.getType().isPrimitive()) {
            throw new IllegalArgumentException("Requirement type for annotation '" + annotation.getName() + "' is a reference types");
        }
    }

    default boolean isFieldIsNull(Object entity, Field entityField) {
        try {
            return Objects.isNull(entityField.get(entity));
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        throw new IllegalArgumentException("bad value");
    }

}
