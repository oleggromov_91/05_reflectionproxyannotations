package com.annotations.validators.entityValidator;

import com.annotations.validators.entityValidatorAnnotations.MinNumberValue;

import java.lang.reflect.Field;

public class MinNumberEntityValidator implements EntityValidator {


    @Override
    public void validate(Object entity, Field entityField) {
        if (!isFieldIsNull(entity, entityField)) {
            typeShouldNumberForAnnotations(entityField, MinNumberValue.class);
            MinNumberValue annotation = entityField.getAnnotation(MinNumberValue.class);
            long validMinValue = annotation.minNumberRequirement();
            long entityFieldValue;
            try {
                entityFieldValue = parseNumber(entityField.get(entity).toString());
                if (entityFieldValue < validMinValue) {
                    throw new IllegalArgumentException("Entity has not valid field '" + entityField.getName()
                            + "' valid min number value is " + validMinValue + " but is " + entityFieldValue);
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }
}
