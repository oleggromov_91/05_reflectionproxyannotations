package com.annotations.validators.entityValidator;

import com.annotations.validators.entityValidatorAnnotations.MaxNumberValue;

import java.lang.reflect.Field;

public class MaxNumberEntityValidator implements EntityValidator {

    @Override
    public void validate(Object entity, Field entityField) {
        if (!isFieldIsNull(entity, entityField)) {
            typeShouldNumberForAnnotations(entityField, MaxNumberValue.class);
            MaxNumberValue annotation = entityField.getAnnotation(MaxNumberValue.class);
            long validMaxValue = annotation.maxNumberRequirement();
            long entityFieldValue;
            try {
                entityFieldValue = parseNumber(entityField.get(entity).toString());
                if (entityFieldValue > validMaxValue) {
                    throw new IllegalArgumentException("Entity has not valid field '" + entityField.getName()
                            + "' valid max number value is " + validMaxValue + " but is " + entityFieldValue);
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

}
