package com.annotations.validators.entityValidator;

import com.annotations.validators.entityValidatorAnnotations.MaxLength;

import java.lang.reflect.Field;

public class MaxLengthEntityValidator implements EntityValidator {
    @Override
    public void validate(Object entity, Field entityField) {
        if (!isFieldIsNull(entity, entityField)) {
            typeShouldStringForAnnotations(entityField, MaxLength.class);
            MaxLength annotation = entityField.getAnnotation(MaxLength.class);
            long validMinValue = annotation.maxLengthRequirement();
            int entityFieldValue;
            try {
                entityFieldValue = entityField.get(entity).toString().length();
                if (entityFieldValue > validMinValue) {
                    throw new IllegalArgumentException("Entity has not valid field '" + entityField.getName()
                            + "' valid max length is " + validMinValue + " but is " + entityFieldValue);
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

}
