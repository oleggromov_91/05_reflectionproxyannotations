package com.annotations.validators.entityValidator;

import com.annotations.validators.entityValidatorAnnotations.MinLength;

import java.lang.reflect.Field;

public class MinLengthEntityValidator implements EntityValidator {

    @Override
    public void validate(Object entity, Field entityField) {
        if (!isFieldIsNull(entity, entityField)) {
            typeShouldStringForAnnotations(entityField, MinLength.class);
            MinLength annotation = entityField.getAnnotation(MinLength.class);
            long validMinValue = annotation.minLengthRequirement();
            int entityFieldValue;
            try {
                entityFieldValue = entityField.get(entity).toString().length();
                if (entityFieldValue < validMinValue) {
                    throw new IllegalArgumentException("Entity has not valid field '" + entityField.getName()
                            + "' valid min length is " + validMinValue + " but is " + entityFieldValue);
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }
}
