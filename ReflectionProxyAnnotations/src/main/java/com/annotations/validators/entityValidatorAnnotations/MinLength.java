package com.annotations.validators.entityValidatorAnnotations;

import java.lang.annotation.*;

@Inherited
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.METHOD})
public @interface MinLength {

    int minLengthRequirement();
}
