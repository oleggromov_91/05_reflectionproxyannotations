package com.annotations.validators.basicValidator;

import com.annotations.validators.entityValidator.*;
import com.annotations.validators.entityValidatorAnnotations.*;

import java.util.Map;

public class ValidatorSet {

    private ValidatorSet() {
    }

    protected static BasicEntityValidator basicValidator = new BasicEntityValidator(

    );

    protected static BasicEntityValidator minNumberValidator = new BasicEntityValidator(
            Map.of(MinNumberValue.class, new MinNumberEntityValidator())
    );


    protected static BasicEntityValidator maxNumberValidator = new BasicEntityValidator(
            Map.of(MaxNumberValue.class, new MaxNumberEntityValidator())
    );


    protected static BasicEntityValidator minLengthValidator = new BasicEntityValidator(
            Map.of(MinLength.class, new MinLengthEntityValidator())
    );


    protected static BasicEntityValidator maxLengthValidator = new BasicEntityValidator(
            Map.of(MaxLength.class, new MaxLengthEntityValidator())
    );

    protected static BasicEntityValidator notNullValidator = new BasicEntityValidator(
            Map.of(NotNull.class, new NotNullEntityValidator())
    );


    protected static BasicEntityValidator notEmptyValidator = new BasicEntityValidator(
            Map.of(NotEmpty.class, new NotEmptyEntityValidator())
    );


}
