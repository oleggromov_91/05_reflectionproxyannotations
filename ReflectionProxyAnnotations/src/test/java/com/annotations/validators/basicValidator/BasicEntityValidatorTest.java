package com.annotations.validators.basicValidator;

import com.annotations.entity.PersonWithBadFields;
import com.annotations.entity.PersonWithGoodFields;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class BasicEntityValidatorTest {

    private final PersonWithBadFields badPerson = new PersonWithBadFields();
    private final PersonWithGoodFields goodPerson = new PersonWithGoodFields();

    /**
     * For entity with bad fields
     */

    @Test
    void passIfFieldHasMinValue() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> ValidatorSet.minNumberValidator.validate(badPerson));
        String actualMessage = exception.getMessage();
        String expectedMessage = "valid min number value is";
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void passIfFieldHasMaxValue() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> ValidatorSet.maxNumberValidator.validate(badPerson));
        String actualMessage = exception.getMessage();
        String expectedMessage = "valid max number value is";
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void passIfFieldHasMinLengthValue() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> ValidatorSet.minLengthValidator.validate(badPerson));
        String actualMessage = exception.getMessage();
        String expectedMessage = "valid min length is ";
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void passIfFieldHasMaxLengthValue() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> ValidatorSet.maxLengthValidator.validate(badPerson));
        String actualMessage = exception.getMessage();
        String expectedMessage = "valid max length is ";
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void passIfFieldHasEmptyValue() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> ValidatorSet.notEmptyValidator.validate(badPerson));
        String actualMessage = exception.getMessage();
        String expectedMessage = "' entity is empty";
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void passIfFieldHasNullValue() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> ValidatorSet.notNullValidator.validate(badPerson));
        String actualMessage = exception.getMessage();
        String expectedMessage = "' entity is null";
        assertTrue(actualMessage.contains(expectedMessage));
    }

    /**
     * For entity with good fields
     */
    @Test
    void passIfAllFieldsIsGood() {
        boolean actualValidationResult = ValidatorSet.basicValidator.validate(goodPerson);
        assertTrue(actualValidationResult);
    }


}