package com.annotations.entity;

import com.annotations.validators.entityValidatorAnnotations.*;

public class PersonWithBadFields {

    @MinNumberValue(minNumberRequirement = 1200)
    private final Double moneyAmount = 11.6436;

    @MaxNumberValue(maxNumberRequirement = 20)
    private final int age = 25;

    @MaxLength(maxLengthRequirement = 5)
    private final String firstName = "Konstantin";

    @MinLength(minLengthRequirement = 3)
    private final String secondName = "Lo";

    @NotEmpty
    private final String middleName = "                 ";

    @NotNull
    private final Object work = null;


}
