package com.annotations.entity;

import com.annotations.validators.entityValidatorAnnotations.*;

public class PersonWithGoodFields {

    @MinNumberValue(minNumberRequirement = 1200)
    private final Double moneyAmount = 11222.6436;

    @MaxNumberValue(maxNumberRequirement = 30)
    private final int age = 25;

    @MaxLength(maxLengthRequirement = 20)
    private final String firstName = "Konstantin";

    @MinLength(minLengthRequirement = 1)
    private final String secondName = "Lo";

    @NotEmpty
    private final String middleName = "           hello)      ";

    @NotNull
    private final Object work = new Object();


}
