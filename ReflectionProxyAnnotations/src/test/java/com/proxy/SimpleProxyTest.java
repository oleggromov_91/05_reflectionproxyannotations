package com.proxy;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

class SimpleProxyTest {

    private final SimpleProxy<Map<String, String>> simpleProxy = new SimpleProxy<>(HashMap::new);
    private final Map<String, String> proxyMap = simpleProxy.proxyMethodLogger();

    @Test
    void printPutMapMethodInConsole() {
        proxyMap.put("One", "Two");
    }

    @Test
    void printGetMapMethodInConsole() {
        proxyMap.get("ghjgyyjg");
    }
}