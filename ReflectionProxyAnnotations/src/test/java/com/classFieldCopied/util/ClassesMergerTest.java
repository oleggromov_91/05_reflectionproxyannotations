package com.classFieldCopied.util;

import com.classFieldCopied.entity.FirstClassWithSameFieldsAsSecond;
import com.classFieldCopied.entity.MergeClassesResult;
import com.classFieldCopied.entity.FirstClass;
import com.classFieldCopied.entity.SecondClass;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import java.lang.reflect.InvocationTargetException;

class ClassesMergerTest {

    private final FirstClass firstClass = new FirstClass("Иван", "Иванов", "Иванович", 18);
    private final SecondClass secondClass = new SecondClass("male", "Ivanov@mail.ru", "+789218392", 150_000);
    private final FirstClassWithSameFieldsAsSecond sameFieldsAsSecond = new FirstClassWithSameFieldsAsSecond("male", "Ivanov@mail.ru", "+789218392", 150_000);


    @Test
    void passIfMergeResultEquals() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        MergeClassesResult expected = new MergeClassesResult("Иван", "Иванов", 18, "+789218392", 150000);
        MergeClassesResult actual = ClassesMerger.merge(firstClass, secondClass, MergeClassesResult.class);

        assertEquals(expected, actual);
    }


    @Test()
    void passIfClassesHasSameFieldName() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> ClassesMerger.merge(sameFieldsAsSecond, secondClass, MergeClassesResult.class));
        String actualMessage = exception.getMessage();
        String expectedMessage = "Classes has same nameFields";
        assertTrue(actualMessage.contains(expectedMessage));
    }

}