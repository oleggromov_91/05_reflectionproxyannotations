package com.classFieldCopied.entity;

import java.util.Objects;

public class MergeClassesResult {

    private String firstName;
    private String secondName;
    private int age;
    private String phone;
    private int moneyAmount;


    public MergeClassesResult(String firstName, String secondName, int age, String phone, int moneyAmount) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.age = age;
        this.phone = phone;
        this.moneyAmount = moneyAmount;
    }

    public MergeClassesResult() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MergeClassesResult that = (MergeClassesResult) o;
        return age == that.age &&
                moneyAmount == that.moneyAmount &&
                Objects.equals(firstName, that.firstName) &&
                Objects.equals(secondName, that.secondName) &&
                Objects.equals(phone, that.phone);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, secondName, age, phone, moneyAmount);
    }

    @Override
    public String toString() {
        return "MergeClassesResult{" +
                "firstName='" + firstName + '\'' +
                ", secondName='" + secondName + '\'' +
                ", age=" + age +
                ", phone='" + phone + '\'' +
                ", moneyAmount=" + moneyAmount +
                '}';
    }
}
