package com.classFieldCopied.entity;

public class FirstClassWithSameFieldsAsSecond {

    private final String sex;
    private final String email;
    private final String phone;
    private final int moneyAmount;

    public FirstClassWithSameFieldsAsSecond(String sex, String email, String phone, int moneyAmount) {
        this.sex = sex;
        this.email = email;
        this.phone = phone;
        this.moneyAmount = moneyAmount;
    }
}
