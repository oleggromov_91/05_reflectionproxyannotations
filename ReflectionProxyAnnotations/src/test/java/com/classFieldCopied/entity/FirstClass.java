package com.classFieldCopied.entity;

public class FirstClass {

    private final String firstName;
    private final String secondName;
    private final String middleName;
    private final int age;

    public FirstClass(String firstName, String secondName, String middleName, int age) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.middleName = middleName;
        this.age = age;
    }
}
